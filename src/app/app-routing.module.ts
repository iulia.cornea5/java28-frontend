import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EventListComponent} from './event-list/event-list.component';
import {EventFormSecondComponent} from './event-form-second/event-form-second.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  // http://localhost:4200/our-events
  {path: 'our-events', component: EventListComponent},
  // http://localhost:4200/new-event
  {path: 'new-event', component: EventFormSecondComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
