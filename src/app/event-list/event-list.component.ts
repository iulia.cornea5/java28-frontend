import {Component, OnInit} from '@angular/core';
import {EventsService} from '../service/events.service';
import {EventDTO} from '../../model/eventDTO';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  // private eventsService: EventsService;
  events: EventDTO[] = [];

  constructor(private eventsService: EventsService, private httpClient: HttpClient) {
    this.eventsService = eventsService;
  }

  ngOnInit(): void {
    // this.httpClient.get('http://localhost:8080/api/events').subscribe(springResponse => {
    //   this.events = springResponse;
    // });

    this.eventsService.getAll().subscribe(successfulResponse => {
      // success = HttpStatus OK
      console.log('Successful response received: ' + successfulResponse);
      this.events = successfulResponse;
    });
  }

}
