import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EventsService} from '../service/events.service';
import {Router} from '@angular/router';
import {EventCreateDTO} from '../../model/eventCreateDTO';

@Component({
  selector: 'app-event-form-second',
  templateUrl: './event-form-second.component.html',
  styleUrls: ['./event-form-second.component.css']
})
export class EventFormSecondComponent implements OnInit {

  eventForm = new FormGroup({
    name: new FormControl(
      '',
      [Validators.required,
        Validators.minLength(3)]),
    dateFormControl: new FormControl('')
  });


  constructor(private eventService: EventsService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onCreateEvent(): void {
    if (!this.eventForm.valid) {
      alert('Invalid form');
      return;
    }

    const eventDto: EventCreateDTO = {
      name: this.eventForm.get('name').value,
      startDate: this.eventForm.get('dateFormControl').value
    };
    this.eventService.create(eventDto).subscribe(response => {
      alert('Object created in backend' + response.toString());
      this.router.navigate(['/our-events']);
    });

  }

}
