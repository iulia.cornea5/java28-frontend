import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EventDTO} from '../../model/eventDTO';
import {EventCreateDTO} from '../../model/eventCreateDTO';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private eventsUrl = '/api/events';

  private httpClient;

  constructor(httpClient: HttpClient, private userService: UserService) {
    this.httpClient = httpClient;
  }

  // call Http GET on http://localhost:8080/api/events and return a list of EventDTO
  public getAll(): Observable<EventDTO[]> {

    return this.httpClient.get(this.eventsUrl, this.userService.optionsWithAuthorizationHeader);
    // return this.httpClient.get(this.eventsUrl);
  }

  // call a POST on http://localhost:8080/api/events with body createDTO and returns the created object
  public create(createDTO: EventCreateDTO): Observable<any> {
    return this.httpClient.post(this.eventsUrl, createDTO, this.userService.optionsWithAuthorizationHeader);
  }

}
