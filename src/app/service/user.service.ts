import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EventDTO} from '../../model/eventDTO';
import {EventCreateDTO} from '../../model/eventCreateDTO';
import {UserLoginDTO} from '../../model/userLoginDTO';
import {UserDTO} from '../../model/userDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = '/api/users';
  private userDTO: UserDTO;
  optionsWithAuthorizationHeader;

  constructor(private httpClient: HttpClient) {

  }

  public login(userLoginDTO: UserLoginDTO): Observable<any> {

    this.optionsWithAuthorizationHeader = {headers: {Authorization: 'Basic ' + btoa(userLoginDTO.email + ':' + userLoginDTO.password)}};

    // POST http://localhost:8080/api/users/login with authorization an userLogin object in body
    return this.httpClient.post(
      this.userUrl + '/login',
      userLoginDTO,
      this.optionsWithAuthorizationHeader);
  }

  getUser(): UserDTO {
    return this.userDTO;
  }

  setUser(user: UserDTO): void {
    this.userDTO = user;
  }

  isAuthenticated(): boolean {
    if (this.userDTO != null) {
      return true;
    }
    return false;
  }


}
