export class UserDTO {

  uuid: string;
  email: string;
  firstName: string;
  lastName: string;
  registrationDate: Date;
  role: string;
  // NO PASSWORD
}
