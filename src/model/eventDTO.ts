export class EventDTO {
  id: string;
  name: string;
  startDate: string;
}
